/**
 * Defines Initializers Application.
 * @Initializers
 */

var fs = require('fs');
var path = require('path');
var events = require('events');
var winston = require('winston');
require('winston-daily-rotate-file');
var config = require('../config')();

module.exports = function() {

  // process.setMaxListeners(0);
  var transport = new (winston.transports.DailyRotateFile)({
    filename: 'logs/log',
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: process.env.ENV === 'development' ? 'debug' : 'info'
  });

  global.logger = new (winston.Logger)({
    transports: [
      transport
    ]
  });

  events.EventEmitter.defaultMaxListeners = Infinity;
  if (process.pid) {
		var path_file = path.resolve(".") + '/tmp/pids/server.pid';
		fs.stat(path_file, function(err, stat) {
			if(err === null) {
				fs.writeFile(path_file, process.pid, function (err) {
          if(err) {
              global.logger.error(err);
          }
        });
	    } else if(err.code == 'ENOENT') {
      	fs.writeFile(path_file, process.pid, function (err) {
          if(err) {
              global.logger.error(err);
          }
        });
	    } else {
	        global.logger.error('Some other error: ', err.code);
	    }
	  });
	}
};
